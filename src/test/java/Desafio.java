import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Desafio {

    private WebDriver driver;

    @Before
    public void abrir (){
        System.setProperty("webdriver.gecko.driver", "C:/ageckodriver/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get("https://testpages.herokuapp.com/styled/basic-html-form-test.html");

    }

    @After
    public void sair() {
        driver.quit();
    }

    @Test
    public void Testedesite() throws InterruptedException {
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[1]/td/input")).sendKeys("bbiacp");
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[2]/td/input")).sendKeys("123456");
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[3]/td/textarea")).sendKeys("Teste de site do desafio");
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[5]/td/input[3]")).click();
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[5]/td/input[2]")).click();
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[6]/td/input[3]")).click();
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[7]/td/select/option[2]")).click();
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[9]/td/input[2]")).click();
        Assert.assertEquals("Submitted Values", driver.findElement(By.xpath("/html/body/div/div[3]/h2")).getText());

    }

    @Test
    public void Testedesite2() throws InterruptedException {
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[1]/td/input")).sendKeys("juju");
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[2]/td/input")).sendKeys("123");
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[3]/td/textarea")).sendKeys("Teste 2");
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[5]/td/input[3]")).click();
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[5]/td/input[1]")).click();
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[6]/td/input[1]")).click();
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[7]/td/select/option[1]")).click();
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[9]/td/input[2]")).click();
        Assert.assertEquals("Submitted Values", driver.findElement(By.xpath("/html/body/div/div[3]/h2")).getText());
    }
}
